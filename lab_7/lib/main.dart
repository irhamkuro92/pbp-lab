import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Create Forum",
    home: ForumForm(),
  ));
}

class ForumForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<ForumForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Invid19"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 10.0),
                  child: Text('Create Forum',
                      style: const TextStyle(
                        fontSize: 40.0,
                        color: Colors.white,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Put your title here ...",
                      labelText: "Title",
                      //           icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Title cannot be empty';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    maxLines: 10,
                    obscureText: false,
                    decoration: new InputDecoration(
                      hintText: "Write your message ...",
                      labelText: "Message",
                      //      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Message cannot be empty';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    "Create",
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
