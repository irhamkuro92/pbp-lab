from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # Implement this (IDK)
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('http://127.0.0.1:8000/lab-3/')

    context = {'form' : form}
    return render(request, 'lab3_form.html', context)

