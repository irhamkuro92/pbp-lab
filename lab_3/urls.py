from django.urls import path
from lab_1.views import friend_list
from lab_3.views import add_friend, index

urlpatterns = [
    path('', index, name='index'),
    # Add friends path using friend_list Views
    path('friends', friend_list, name='friend_list'),
    path('add', add_friend, name='add_friend'),
]
