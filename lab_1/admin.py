from django.contrib import admin
from lab_1.models import Friend

# Register Friend model here
admin.site.register(Friend)
