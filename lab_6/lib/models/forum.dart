import 'package:flutter/material.dart';

class Forum {
  final String title;
  final String desk;
  final String creator;
  final String waktu;

  const Forum({
    required this.title,
    required this.creator,
    required this.desk,
    required this.waktu,
  });
}
