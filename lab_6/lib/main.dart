import 'package:flutter/material.dart';
import 'package:lab_6/dummy_data.dart';
import 'package:lab_6/models/forum.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  List<Forum> _dummyForum = DUMMY_FORUM;

  Widget getForumWidgets(List<Forum> forumList) {
    List<Widget> list = [
      Padding(
        padding: EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 10.0),
        child: Text('Forum Diskusi',
            style: const TextStyle(
              fontSize: 40.0,
              color: Colors.white,
            )),
      ),
    ];
    for (var i = 0; i < forumList.length; i++) {
      var title = forumList[i].title;
      var creator = forumList[i].creator;
      var desk = forumList[i].desk;
      var waktu = forumList[i].waktu;

      list.add(Container(
          margin: new EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          child: Card(
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {
                //  print('Card tapped.');
              },
              child: SizedBox(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(title,
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 24.0,
                            ),
                            textAlign: TextAlign.justify),
                        const Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0)),
                        Text(creator + " - " + waktu,
                            style: const TextStyle(
                              fontSize: 16.0,
                              color: Colors.grey,
                            ),
                            textAlign: TextAlign.justify),
                        const Padding(
                            padding: EdgeInsets.symmetric(vertical: 5.0)),
                        Text(desk,
                            style: const TextStyle(fontSize: 18.0),
                            textAlign: TextAlign.justify),
                      ],
                    ),
                  ),
                  // color: Colors.white,
                ),
              ),
            ),
          )));
    }
    return new ListView(children: list);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Invid19',
      theme: ThemeData(canvasColor: Colors.grey.shade900),
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Invid19'),
            backgroundColor: Colors.blueGrey[800],
          ),
          body: getForumWidgets(_dummyForum)),
    );
  }
}

// Column(
//           children: [
//             Padding(
//               padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
//               child: Text('Forum Diskusi',
//                   style: const TextStyle(
//                     fontSize: 40.0,
//                     color: Colors.white,
//                   )),
//             ),
//             getForumWidgets(_dummyForum)
//           ],
//         ),