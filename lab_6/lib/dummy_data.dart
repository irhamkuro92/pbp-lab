import 'package:flutter/material.dart';

import './models/forum.dart';

const DUMMY_FORUM = [
  Forum(
      title: 'Weekly Contact',
      creator: 'Irham',
      desk: 'Qapla!',
      waktu: 'Saturday, 30 October 2021, 05:29 PM'),
  Forum(
      title: 'PBP',
      creator: 'guest',
      desk: 'HAHAHA',
      waktu: '02 November 2021, 11:28 AM'),
  Forum(
      title: 'Virus Covid',
      creator: 'Admin',
      desk:
          'COVID-19 adalah penyakit yang disebabkan oleh virus severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). COVID-19 dapat menyebabkan',
      waktu: 'Saturday, 30 October 2021, 03:43 PM'),
];
