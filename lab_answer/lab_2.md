## Perbedaan JSON dengan XML
Berikut perbedaan-perbedaan di antara JSON dan XML:
 
| JSON                         	| XML                          	|
|------------------------------	|------------------------------	|
| Lebih tidak aman             	| Lebih aman dibandingkan JSON 	|
| Tidak mensupport comments    	| Mensupport comments          	|
| Hanya support UTF-8 encoding 	| Support berbagai encoding    	|
| Support array                	| Tidak mensupport array       	|
| Tidak mensupport namespace   	| support namespace            	|

## Perbedaan HTML dengan XML
Berikut perbedaan-perbedaan di antara HTML dan XML:
| XML                                                                          	| HTML                                                                      	|
|------------------------------------------------------------------------------	|---------------------------------------------------------------------------	|
| Bentuk lengkapnya adalah ekstensi dari Markup Language                       	| Bentuk lengkapnya adalah Hypertext Markup Language                        	|
| Tujuan utamanya adalah untuk fokus pada pengiriman data dan penyimpanan data 	| Berfokus pada tampilan data. Meningkatkan tampilan teks                   	|
| XML bersifat dinamis karena digunakan dalam pengiriman data                  	| HTML bersifat statis karena fungsi utamanya adalah untuk menampilkan data 	|
| Case sensitive. Huruf besar dan kecil perlu diperhatikan saat pengkodean     	| Case insensitive. Huruf besar dan kecil tidak penting saat pengkodean     	|
| Setiap kesalahan dalam kode tidak akan memberikan hasil akhir                	| Kesalahan kecil dalam pengkodean dapat diabaikan dan hasilnya tetap ada   	|

Sekian Jawaban saya kurang lebihnya mohon maaf, Terima kasih<br>
<br>
Salam,<br>
Muhammad Irham Luthfi

<br>

Referensi <br>
- https://www.geeksforgeeks.org/difference-between-json-and-xml/ 
- https://byjus.com/free-ias-prep/difference-between-xml-and-html/